﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UdpServerDetectionForm.Models;
using UdpServerDetectionForm.Networking;
using Newtonsoft.Json;

namespace UdpServerDetectionForm
{
    public partial class Form1 : Form
    {
        ServerListener server;
        ClientSender client;


        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            server = new ServerListener(9999);
            //this event we use it  when the serverListener receive data from client , you should return string as response 
            //and the client will derialize the string to the real object
            server.OnReceiveRequestFromClient += Server_OnReceiveRequestFromClient;
            await server.StartListen(true);
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            client = new ClientSender(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9999));
            //this event we use it when the clientsender receive the response from server
            client.OnReceiveResponseFromServer += Tt_OnReceiveResponseFromServer; ;
            //the second parameter tell the sender to wait for response or not
            await client.Send(JsonConvert.SerializeObject( new ClientMessage { clientData = "FromClient" }),true);
        }


        private void Tt_OnReceiveResponseFromServer(string data)
        {
            //deserlize the response string to the real object (your type)
            var realResponse = JsonConvert.DeserializeObject<ServerMessage>(data);
            MessageBox.Show("Response from server:" + realResponse.serverData);
        }



        private string Server_OnReceiveRequestFromClient(string data)
        {
            //deserlize the request string to the real object (your type)
            var realRequest = JsonConvert.DeserializeObject<ClientMessage>(data);
            MessageBox.Show("Response from client:" + realRequest.clientData);
            return JsonConvert.SerializeObject( new ServerMessage { serverData="Hi From Server"});
        }


        private void button3_Click(object sender, EventArgs e)
        {
            if (server == null)
                return;

            //you HAVE TO stop the listening
            server.StopListen();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (client == null)
                return;


            //you HAVE TO stop the listening if you pass true as second paramater of (clientsender)
            client.StopWaitingForResponse();
        }

    }
}
