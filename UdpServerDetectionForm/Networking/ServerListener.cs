﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UdpServerDetectionForm.Models;
using Newtonsoft.Json;


namespace UdpServerDetectionForm.Networking
{
    class ServerListener
    {
        public int port { get; private set; }


        public delegate string UdpReceivedData(string data) ;
        public event UdpReceivedData OnReceiveRequestFromClient;
        bool State { get; set; } = false;

        public ServerListener(int portToListen)
        {
            this.port = portToListen;
        }

        public async Task StartListen(bool sendResponse)
        {
            UdpClient client = new UdpClient(port);
            State = true;

            while(true)
            {
                if (!State)
                    return ;

                var result =await  client.ReceiveAsync();
               
                try
                {
                    var obj = Encoding.UTF8.GetString(result.Buffer, 0, result.Buffer.Length);

                    if (!State)
                    {
                        client.Dispose();
                        return;
                    }


                    var response = OnReceiveRequestFromClient.Invoke(obj);

                    if (sendResponse)
                    {                   
                        var responseAsBytes = Encoding.UTF8.GetBytes(response);
                        client.Send(responseAsBytes, responseAsBytes.Length, result.RemoteEndPoint);
                    }

                   
                }
                catch { }
            }
             
        }

        public void StopListen()
        {
            if (State)
                State = false;

            UdpClient client = new UdpClient();
            var response = JsonConvert.SerializeObject("stop");
            State = false;
            client.Send(Encoding.UTF8.GetBytes(response), response.Length,new IPEndPoint(IPAddress.Loopback,port));
        }

    }
}
