﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UdpServerDetectionForm.Models;
using Newtonsoft.Json;




namespace UdpServerDetectionForm.Networking
{
    class ClientSender
    {

        public int port { get; private set; }
        public IPEndPoint serverEndPoint { get; set; }
        UdpClient client;


        public delegate void UdpReceivedData(string data);
        public event UdpReceivedData OnReceiveResponseFromServer;
        bool State { get; set; } = false;

        public ClientSender(IPEndPoint serverEndPoint)
        {
            this.serverEndPoint = serverEndPoint;
        }

        public async Task Send(string data,bool waitForResponse)
        {          
            client = new UdpClient();
            var requestAsBytes = Encoding.UTF8.GetBytes(data);
            client.Send(requestAsBytes, data.Length, serverEndPoint);


            if (!waitForResponse)
                return ;


            State = true;


            while (true)
            {
                var result = await client.ReceiveAsync();

                if (!State)
                {
                    client.Dispose();
                    return;
                }

                try
                {
                    var x = Encoding.UTF8.GetString(result.Buffer, 0, result.Buffer.Length);

                    if (!State)
                        return;

                    OnReceiveResponseFromServer.Invoke(x);
                }
                catch { }                              
            }
        }

        public void StopWaitingForResponse()
        {
            State = false;
            UdpClient stopClient = new UdpClient();
            var x = Encoding.UTF8.GetBytes("Stop");
            client.Send(x, x.Length,"127.0.0.1", ((IPEndPoint)client.Client.LocalEndPoint).Port);       
        }

    }
}
